# Users-Service

This is Authentication service for Donation Apps.
I use token as Authorization, Sanctum Token.

# How The Authentication Works

In this service, Have 3 feature:
<li> Register
<li> Login
<li> Logout

On Register, client must fill 3 body = name, email and password <br>
On Login, client must fill 2 body = email and password <br>
On Logout, client must send user_id on the Header Authorization for removing token.

At Login, users-service will save token to 2 places, to main database and to redis, saving token to redis is for authentication for other service, other service will check token from client and token on redis, if different will return 'Unauthorized', if same will return the service features.
To check that token, on Campaign-service i use middleware as checker and on Donation-service i use interceptor for checker, but in that service there are some litle bit bugs.