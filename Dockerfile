# Menggunakan image PHP versi terbaru
FROM php:8.1-cli

# Menyalin file composer.lock dan composer.json ke dalam container
COPY composer.lock composer.json /var/www/users/

# Menentukan direktori kerja
WORKDIR /var/www/users

# Menginstall dependensi aplikasi menggunakan composer
RUN apt-get update && \
    apt-get install -y git zip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer install --no-scripts --no-autoloader && \
    rm -rf /var/lib/apt/lists/*

# Menyalin seluruh kode sumber aplikasi ke dalam container
COPY . /var/www/users/

# Menjalankan perintah artisan serve saat container dijalankan
CMD php artisan serve --host=0.0.0.0 --port=8000
