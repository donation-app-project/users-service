<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string|min:6',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            if ($user) {
                $response = [
                    'status_code'   => 200,
                    'data'          => $user,
                    'message'       => 'Success create account !'
                ];
                return response()->json($response, $response['status_code']);
            } else {
                $response = [
                    'status_code'   => 404,
                    'data'          => $user,
                    'message'       => 'Failed create account !'
                ];
                return response()->json($response, $response['status_code']);
            }
        } catch(\Throwable $th) {
            $response = [
                'status_code'   => 500,
                'message'       => $th->getMessage()
            ];
            return response()->json($response, $response['status_code']);
        }
    }

    public function login(Request $request)
    {
        try {
           $credentials = $request->only('email', 'password');

            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                $token = $user->createToken("token" . " " . $user->name)->plainTextToken;
                $token_to_redis = Redis::set('token' . '_' . $user->name, $token);
                // Token berhasil dihasilkan
               if($token_to_redis){
                   return response()->json([
                       'message' => 'Success Login',
                   ]);
               } else {
                   return response()->json([
                       'message' => 'failed Login',
                   ], 400);
               }
            }

            // Otentikasi gagal
            return response()->json(['error' => 'Unauthorized'], 401);
        } catch(\Throwable $th) {
            $response = [
                'status_code'   => 500,
                'message'       => $th->getMessage()
            ];
            return response()->json($response, $response['status_code']);
        }

    }

    public function logout(Request $request)
    {
       try {
           $user_id = $request->bearerToken();
           $query = "SELECT * FROM personal_access_tokens WHERE tokenable_id = ?";
           $check = DB::select($query, [$user_id]);
           if ($check) {
               Redis::del('token' . '_' . $check[0]->name);
               $response = [
                 'status_code'  => 200,
                 'message'      => 'Logout success'
               ];
               return response()->json($response, $response['status_code']);
           } else {
               $response = [
                   'status_code'    => 404,
                   'message'        => 'Logout failed'
               ];
               return response()->json($response, $response['status_code']);
           }
       } catch (\Throwable $th) {
           $response = [
               'status_code'    => 500,
               'message'        => $th->getMessage()
           ];
           return response()->json($response, $response['status_code']);
       }
    }
}

